
<section>
    <form action="index.php?page=salvar_doacao=cadastrar" method="POST">
        <h3>Dados Pessoais</h3>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputEmail4">Nome</label>
                <input type="text" name="nomeDoador" class="form-control" id="inputEmail4" placeholder="Nome">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEmail4">Sobrenome</label>
                <input type="text" name="SobnomeDoador" class="form-control" placeholder="Sobnome">
            </div>

            <div class="form-group col-md-4">
                <label for="inputEmail4">Email</label>
                <input type="email" name="emailDoador" class="form-control" id="inputEmail4" placeholder="Email">
            </div>
        </div>
        <hr>
        <h3>Endereço</h3>
        <div class="form-group">
            <label for="inputAddress">Endereço</label>
            <input type="text" class="form-control" id="inputAddress" placeholder="Rua dos Bobos, nº 0">
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputCity">Cidade</label>
                <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-4">
                <label for="inputEstado">Estado</label>
                <select id="inputEstado" class="form-control">
                    <option selected>Escolher...</option>
                    <option>DF</option>
                    <option>GO</option>
                    <option>MG</option>
                </select>
            </div>
            <div class="form-group col-md-2">
                <label for="inputCEP">CEP</label>
                <input type="text" class="form-control" id="inputCEP">
            </div>
        </div>
        <div class="form-group">
            <hr>
            <h3>Informaçõe adicionais</h3>
            <label for="exampleFormControlTextarea1">Mensagem</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>

</section>
